First Step:

Copy rtl8812au-5.2.9 directory into /usr/src/rtl8812au-5.2.9

Second Step:

cd into /usr/src... and perform dkms install using =>

	sudo dkms add -m rtl8812au -v 5.2.9
	sudo dkms build -m rtl8812au -v 5.2.9
	sudo dkms install -m rtl8812au -v 5.2.9

Third Step (?):

Modify /etc/NetworkManager/NetworkManager.conf file by appending =>

[device]
wifi.scan-rand-mac-address=no
